import React, {Component} from "react";
import { SafeAreaView, StyleSheet, TextInput, Button, Text } from "react-native";


class App extends Component {
  state = {
    kataAwal:"",
    kataAkhir:""
  }
  kataAwalTextChange = (inputText) =>{
    this.setState({kataAwal : inputText})
  }

  kataAkhirTextChange = (inputText) =>{
    this.setState({kataAkhir : inputText})
  }

  hasil = () => {
    alert (this.state.kataAwal + this.state.kataAkhir)
  }
  render() {
    return (
      <SafeAreaView>
        <TextInput
          style={styles.input}
          onChangeText={this.kataAwalTextChange}
          placeholder = "masukkan kata awal"
        />
        <TextInput
          style={styles.input}
          onChangeText={this.kataAkhirTextChange}
          placeholder = "masukkan kata akhir"
        />
        <Button
          title="click on"
          onPress={this.hasil}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    height: 30,
    margin: 8,
    borderWidth: 1,
    padding: 2,
  },
});

export default App;